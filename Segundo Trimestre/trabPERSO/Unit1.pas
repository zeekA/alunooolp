unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.StdCtrls,
  Vcl.Menus, Unit2;

type
  TForm1 = class(TForm)
    Button1: TButton;
    NetHTTPClient1: TNetHTTPClient;
    ListBox1: TListBox;
    MainMenu1: TMainMenu;
    Criaodepersonagem1: TMenuItem;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Criaodepersonagem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TPersonagem = class(TObject)
    nome, profissao: String;
    nivel, ataque, defesa, vida: integer;
  end;

var
  Form1: TForm1;
  Personagem: TPersonagem;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  conteudo: String;
  liPersonagens: TArray<String>;
  liAtributos: TArray<String>;
  strPersonagens: String;
begin
  conteudo := NetHTTPClient1.Get('https://venson.net.br/ws/personagens')
  .ContentAsString();
    liPersonagens := conteudo.Split(['&']);


  for strPersonagens in liPersonagens do
  begin
    Personagem := TPersonagem.Create;
    liAtributos := strPersonagens.Split([';']);

    Personagem.nome := liAtributos[0];
    Personagem.profissao := liAtributos[1];
    Personagem.nivel := liAtributos[2].toInteger;
    Personagem.ataque := liAtributos[3].toInteger;
    Personagem.defesa := liAtributos[4].toInteger;
    Personagem.vida := liAtributos[5].toInteger;

    ListBox1.Items.AddObject(Personagem.nome, Personagem);

  end;

end;


procedure TForm1.Criaodepersonagem1Click(Sender: TObject);
begin
  Form2.ShowModal;
end;

procedure TForm1.ListBox1DblClick(Sender: TObject);
var
  Personagem: TPersonagem;
begin
  Personagem := TPersonagem(ListBox1.Items.Objects[ListBox1.ItemIndex]);
  ShowMessage(Personagem.nome + ' ' + Personagem.profissao + ' ' +
    inttostr (Personagem.nivel));
end;



end.
